#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "IDLCallbacks.h"
#import "IDLConfig.h"
#import "IDLConstants.h"
#import "IDLEncodingUtils.h"
#import "IDLError.h"
#import "IDLEvent.h"
#import "IDLLog.h"
#import "IDLPreferenceHelper.h"
#import "InstalDL.h"
#import "IDLNetworkService.h"
#import "IDLServerRequest.h"
#import "IDLServerResponse.h"

FOUNDATION_EXPORT double InstalDLVersionNumber;
FOUNDATION_EXPORT const unsigned char InstalDLVersionString[];

