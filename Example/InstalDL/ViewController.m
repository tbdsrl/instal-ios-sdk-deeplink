//
//  ViewController.m
//  InstalDL-TestBed
//
//  Created by Leonardo Passeri on 14/02/2018.
//  Copyright © 2018 Balzo. All rights reserved.
//

#import "ViewController.h"
#import <InstalDL/InstalDL.h>

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UITextField *eventTypeTextField;
@property (weak, nonatomic) IBOutlet UITextField *eventValueTextField;
@property (weak, nonatomic) IBOutlet UIButton *sendEventButton;
@property (weak, nonatomic) IBOutlet UITextView *textView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self.sendEventButton addTarget:self action:@selector(sendEvent:) forControlEvents:UIControlEventTouchUpInside];
    
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    tap.cancelsTouchesInView = YES;
    [self.view addGestureRecognizer:tap];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)handleRoutingParams:(NSDictionary *)handleRoutingParams withError:(NSError *)error {
    
    NSString * text = @"";
    if (handleRoutingParams) {
        NSData *paramsJsonData = [NSJSONSerialization dataWithJSONObject:handleRoutingParams
                                                                 options:NSJSONWritingPrettyPrinted
                                                                   error:nil];
        NSString * paramsJsonString = [[NSString alloc] initWithData:paramsJsonData encoding:NSUTF8StringEncoding];
        
        
        text = [text stringByAppendingString:[NSString stringWithFormat:@"Routing Params:\n%@", paramsJsonString]];
    }
    if (error) {
        text = [text stringByAppendingString:[NSString stringWithFormat:@"\nError: %@", [error localizedDescription]]];
    }
    
    self.textView.text = text;
}

-(void)sendEvent:(UIButton *)button {
    
    NSString * eventType = self.eventTypeTextField.text;
    
    NSNumberFormatter *formatter=[[NSNumberFormatter alloc]init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    NSNumber *eventValue = [formatter numberFromString:self.eventValueTextField.text];
    
    if ([eventType length]) {
        
//        eventType = [NSString stringWithFormat:@"%@%@", @"organic_", eventType];
//        eventType = [NSString stringWithFormat:@"%@%@", @"dpl_instal_", eventType];
        
        [[InstalDL getInstance] sendEventType:eventType withValue:eventValue];
        self.textView.text = @"Event sent";
    } else {
        self.textView.text = @"Please type an event type";
    }
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

@end

