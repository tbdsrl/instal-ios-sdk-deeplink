//
//  ViewController.h
//  InstalDL-TestBed
//
//  Created by Leonardo Passeri on 14/02/2018.
//  Copyright © 2018 Balzo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

-(void)handleRoutingParams:(NSDictionary *)handleRoutingParams withError:(NSError *)error;

@end


