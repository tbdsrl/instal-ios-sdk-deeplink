//
//  main.m
//  InstalDL
//
//  Created by passeri.leonardo@gmail.com on 02/27/2018.
//  Copyright (c) 2018 passeri.leonardo@gmail.com. All rights reserved.
//

@import UIKit;
#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
