//
//  InstalDL_UriSchemeTest.m
//  InstalDL-TestBedTests
//
//  Created by Leonardo Passeri on 22/02/2018.
//  Copyright © 2018 Balzo. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import <InstalDL/InstalDL.h>
#import <InstalDL/IDLPreferenceHelper.h>
#import <InstalDL/IDLNetworkService.h>
#import <InstalDL/IDLServerResponse.h>
#import <InstalDL/IDLConstants.h>
#import <InstalDL/IDLError.h>

@interface IDLNetworkService (Test)
-(NSURLSession *)getUrlSession;
@end

@interface InstalDL_UriSchemeTest : XCTestCase

@end

@implementation InstalDL_UriSchemeTest {
    id _preferenceHelperMock;
    id _networkServiceMock;
    id _urlSessionMock;
    InstalDL * _instalDL;
}

- (void)setUp {
    [super setUp];
    
    _preferenceHelperMock = OCMPartialMock([IDLPreferenceHelper preferenceHelper]);
    OCMStub([_preferenceHelperMock firstInstall]).andReturn(YES);
    OCMStub([_preferenceHelperMock setFirstInstall:NO]);
    
    _urlSessionMock = OCMClassMock([NSURLSession class]);
    
    _networkServiceMock = OCMPartialMock([IDLNetworkService new]);
    OCMStub([_networkServiceMock getUrlSession]).andReturn(_urlSessionMock);
    
    _instalDL = [[InstalDL alloc] initWithInterface:_networkServiceMock preferenceHelper:_preferenceHelperMock];
}

- (void)tearDown {
    
    [_preferenceHelperMock stopMocking];
    [_networkServiceMock stopMocking];
    [_urlSessionMock stopMocking];
    
    [super tearDown];
}

- (void)testNewAppDelegateMethod
{
    NSHTTPURLResponse * response = [[NSHTTPURLResponse alloc] initWithURL:[NSURL URLWithString:@""] statusCode:200 HTTPVersion:nil headerFields:nil];
    NSData * data = [NSJSONSerialization dataWithJSONObject:@{IDL_RESPONSE_KEY_PARAMS : @{}} options:NSJSONWritingPrettyPrinted error:nil];
    
    [self expectUrlSessionResponse:response data:data error:nil];
    
    XCTestExpectation *expectation = [self expectationWithDescription:@"New AppDelegate Method"];
    
    [_instalDL initSessionWithLaunchOptions:nil andRegisterDeepLinkHandler:^(NSDictionary * _Nullable params, NSError * _Nullable error) {
        
        XCTAssertNil(error);
        XCTAssert(params);
        
        [expectation fulfill];
    }];
    
    XCTAssertTrue([_instalDL application:nil openURL:[NSURL URLWithString:@"instalCustomURLScheme://?dl_click_id=86b91ae1-b442-4ba0-b114-5a5e476828ed:1adc3cd92cb0280e9cee99cbc7cf24866586f387"] options:@{}]);
    
    [self waitForExpectationsWithTimeout:10.0 handler:nil];
}

- (void)testSuccessCallback
{
    NSHTTPURLResponse * response = [[NSHTTPURLResponse alloc] initWithURL:[NSURL URLWithString:@""] statusCode:200 HTTPVersion:nil headerFields:nil];
    NSData * data = [NSJSONSerialization dataWithJSONObject:@{IDL_RESPONSE_KEY_PARAMS : @{}} options:NSJSONWritingPrettyPrinted error:nil];
    
    [self expectUrlSessionResponse:response data:data error:nil];
    
    XCTestExpectation *expectation = [self expectationWithDescription:@"Deep Link Success"];
    
    [_instalDL initSessionWithLaunchOptions:nil andRegisterDeepLinkHandler:^(NSDictionary * _Nullable params, NSError * _Nullable error) {
        
        XCTAssertNil(error);
        XCTAssert(params);
        
        [expectation fulfill];
    }];
    
    XCTAssertTrue([_instalDL application:nil openURL:[NSURL URLWithString:@"instalCustomURLScheme://?dl_click_id=86b91ae1-b442-4ba0-b114-5a5e476828ed:1adc3cd92cb0280e9cee99cbc7cf24866586f387"] sourceApplication:nil annotation:nil]);
    
    [self waitForExpectationsWithTimeout:10.0 handler:nil];
}

- (void)testFailureWrongSchemaCallback
{
    id instalDlMock = OCMPartialMock(_instalDL);
    
    [instalDlMock initSessionWithLaunchOptions:nil andRegisterDeepLinkHandler:nil];
    
    XCTAssertFalse([instalDlMock application:nil openURL:[NSURL URLWithString:@"instalCustomURLScheme://"] sourceApplication:nil annotation:nil]);
}

- (void)testFailureServerError
{
    NSHTTPURLResponse * response = [[NSHTTPURLResponse alloc] initWithURL:[NSURL URLWithString:@""] statusCode:500 HTTPVersion:nil headerFields:nil];
    
    [self expectUrlSessionResponse:response data:nil error:nil];
    
    XCTestExpectation *expectation = [self expectationWithDescription:@"Failure server error"];
    
    [_instalDL initSessionWithLaunchOptions:nil andRegisterDeepLinkHandler:^(NSDictionary * _Nullable params, NSError * _Nullable error) {
        
        XCTAssert(error);
        XCTAssertEqual(error.code, IDLServerProblemError);
        XCTAssertNil(params);
        
        [expectation fulfill];
    }];
    
    XCTAssertTrue([_instalDL application:nil openURL:[NSURL URLWithString:@"instalCustomURLScheme://?dl_click_id=86b91ae1-b442-4ba0-b114-5a5e476828ed:1adc3cd92cb0280e9cee99cbc7cf24866586f387"] sourceApplication:nil annotation:nil]);
    
    [self waitForExpectationsWithTimeout:10.0 handler:nil];
}

-(void)expectUrlSessionResponse:(NSHTTPURLResponse *)response data:(NSData *)data error:(NSError *)error {
    
    typedef void (^dataTaskCallback) (NSData *data, NSURLResponse *urlResponse, NSError *error);
    
    __block dataTaskCallback urlSessionCallback;
    id urlSessionCallbackCheckBlock = [OCMArg checkWithBlock:^BOOL(dataTaskCallback callback) {
        urlSessionCallback = callback;
        return YES;
    }];
    
    id urlSessionCallbackInvocation = ^(NSInvocation *invocation) {
        urlSessionCallback(data, response, error);
        
    };
    
    [[[_urlSessionMock expect]
        andDo:urlSessionCallbackInvocation] dataTaskWithRequest:[OCMArg any] completionHandler:urlSessionCallbackCheckBlock];
}

@end
