//
//  InstalDL_InstallationTests.m
//  InstalDL_InstallationTests
//
//  Created by Leonardo Passeri on 14/02/2018.
//  Copyright © 2018 Balzo. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import <InstalDL/InstalDL.h>
#import <InstalDL/IDLPreferenceHelper.h>
#import <InstalDL/IDLNetworkService.h>
#import <InstalDL/IDLServerResponse.h>
#import <InstalDL/IDLConstants.h>
#import <InstalDL/IDLError.h>

@interface IDLNetworkService (Test)
-(NSURLSession *)getUrlSession;
@end

@interface InstalDL_InstallationTests : XCTestCase

@end

@implementation InstalDL_InstallationTests {
    id _preferenceHelperMock;
    id _networkServiceMock;
    id _urlSessionMock;
    InstalDL * _instalDL;
}

- (void)setUp {
    [super setUp];
    
    _preferenceHelperMock = OCMPartialMock([IDLPreferenceHelper preferenceHelper]);
    OCMStub([_preferenceHelperMock firstInstall]).andReturn(YES);
    OCMStub([_preferenceHelperMock setFirstInstall:NO]);
    
    _urlSessionMock = OCMClassMock([NSURLSession class]);
    
    _networkServiceMock = OCMPartialMock([IDLNetworkService new]);
    OCMStub([_networkServiceMock getUrlSession]).andReturn(_urlSessionMock);
    
    _instalDL = [[InstalDL alloc] initWithInterface:_networkServiceMock preferenceHelper:_preferenceHelperMock];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [_preferenceHelperMock stopMocking];
    [_networkServiceMock stopMocking];
    [_urlSessionMock stopMocking];
    
    [super tearDown];
}

- (void)testSuccessDeepLink
{
    NSHTTPURLResponse * response = [[NSHTTPURLResponse alloc] initWithURL:[NSURL URLWithString:@""] statusCode:200 HTTPVersion:nil headerFields:nil];
    NSData * data = [NSJSONSerialization dataWithJSONObject:@{IDL_RESPONSE_KEY_PARAMS : @{}} options:NSJSONWritingPrettyPrinted error:nil];
    
    [self expectUrlSessionResponse:response data:data error:nil];
    
//    IDLServerResponse *openInstallResponse = [[IDLServerResponse alloc] init];
//    openInstallResponse.statusCode = 200;
//    openInstallResponse.data = @{IDL_RESPONSE_KEY_PARAMS : @{}};
    
    
//    [self expectNetworkServiceResponse:openInstallResponse error:nil];
    
    XCTestExpectation *expectation = [self expectationWithDescription:@"First Installation success Deeplink"];
    
    [_instalDL initSessionWithLaunchOptions:nil andRegisterDeepLinkHandler:^(NSDictionary * _Nullable params, NSError * _Nullable error) {
        
        XCTAssertNil(error);
        XCTAssert(params);
        
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:10.0 handler:nil];
}

- (void)testSuccessOrganic
{
    NSHTTPURLResponse * response = [[NSHTTPURLResponse alloc] initWithURL:[NSURL URLWithString:@""] statusCode:404 HTTPVersion:nil headerFields:nil];
    NSData * data = [NSJSONSerialization dataWithJSONObject:@{@"description": @"Data not found for this user."} options:NSJSONWritingPrettyPrinted error:nil];
    
    [self expectUrlSessionResponse:response data:data error:nil];
    
//    IDLServerResponse *openInstallResponse = [[IDLServerResponse alloc] init];
//    openInstallResponse.statusCode = 400;
//    openInstallResponse.data = @{};
//
//    [self expectNetworkServiceResponse:openInstallResponse error:nil];
    
    XCTestExpectation *expectation = [self expectationWithDescription:@"First Installation success Organic"];
    
    [_instalDL initSessionWithLaunchOptions:nil andRegisterDeepLinkHandler:^(NSDictionary * _Nullable params, NSError * _Nullable error) {
        
        XCTAssertNil(error);
        XCTAssertNil(params);
        
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:10.0 handler:nil];
}

- (void)testFailureServerError
{
    NSHTTPURLResponse * response = [[NSHTTPURLResponse alloc] initWithURL:[NSURL URLWithString:@""] statusCode:500 HTTPVersion:nil headerFields:nil];
    
    [self expectUrlSessionResponse:response data:nil error:nil];
    
//    IDLServerResponse *openInstallResponse = [[IDLServerResponse alloc] init];
//    openInstallResponse.statusCode = 500;
//    openInstallResponse.data = nil;
//
//    [self expectNetworkServiceResponse:openInstallResponse error:[NSError idlErrorWithCode:IDLServerProblemError]];
    
    XCTestExpectation *expectation = [self expectationWithDescription:@"First Installation failure server error"];
    
    [_instalDL initSessionWithLaunchOptions:nil andRegisterDeepLinkHandler:^(NSDictionary * _Nullable params, NSError * _Nullable error) {
        
        XCTAssert(error);
        XCTAssertEqual(error.code, IDLServerProblemError);
        XCTAssertNil(params);
        
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:10.0 handler:nil];
}

- (void)testFailureUnknownError
{
    NSHTTPURLResponse * response = [[NSHTTPURLResponse alloc] initWithURL:[NSURL URLWithString:@""] statusCode:401 HTTPVersion:nil headerFields:nil];
    
    [self expectUrlSessionResponse:response data:nil error:nil];
    
//    IDLServerResponse *openInstallResponse = [[IDLServerResponse alloc] init];
//    openInstallResponse.statusCode = 401;
//    openInstallResponse.data = nil;
//
//    [self expectNetworkServiceResponse:openInstallResponse error:[NSError idlErrorWithCode:IDLUnexpectedError]];
    
    XCTestExpectation *expectation = [self expectationWithDescription:@"First Installation failure unknown error"];
    
    [_instalDL initSessionWithLaunchOptions:nil andRegisterDeepLinkHandler:^(NSDictionary * _Nullable params, NSError * _Nullable error) {
        
        XCTAssert(error);
        XCTAssertEqual(error.code, IDLUnexpectedError);
        XCTAssertNil(params);
        
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:10.0 handler:nil];
}

- (void)testFailureConnectivityError
{
    NSError * error = [NSError errorWithDomain:NSURLErrorDomain code:NSURLErrorCannotFindHost userInfo:nil];
    
    [self expectUrlSessionResponse:nil data:nil error:error];
    
    //    IDLServerResponse *openInstallResponse = [[IDLServerResponse alloc] init];
    //    openInstallResponse.statusCode = 666;
    //    openInstallResponse.data = nil;
    //
    //    [self expectNetworkServiceResponse:openInstallResponse error:[NSError idlErrorWithCode:IDLUnexpectedError]];
    
    XCTestExpectation *expectation = [self expectationWithDescription:@"First Installation failure connectivity error"];
    
    [_instalDL initSessionWithLaunchOptions:nil andRegisterDeepLinkHandler:^(NSDictionary * _Nullable params, NSError * _Nullable error) {
        
        XCTAssert(error);
        XCTAssertEqual(error.code, IDLConnectivityError);
        XCTAssertNil(params);
        
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:10.0 handler:nil];
}

#pragma mark - Utility Methods

-(void)expectNetworkServiceResponse:(IDLServerResponse *)response error:(NSError *)error {
    
    __block IDLServerCallback openOrInstallCallback;
    id openOrInstallCallbackCheckBlock = [OCMArg checkWithBlock:^BOOL(IDLServerCallback callback) {
        openOrInstallCallback = callback;
        return YES;
    }];
    
    id openOrInstallInvocation = ^(NSInvocation *invocation) {
        openOrInstallCallback(response, error);
    };
    
    [[[_networkServiceMock expect]
      andDo:openOrInstallInvocation]
     sendRequest:[OCMArg any] callback:openOrInstallCallbackCheckBlock];
}

-(void)expectUrlSessionResponse:(NSHTTPURLResponse *)response data:(NSData *)data error:(NSError *)error {
    
    typedef void (^dataTaskCallback) (NSData *data, NSURLResponse *urlResponse, NSError *error);
    
    __block dataTaskCallback urlSessionCallback;
    id urlSessionCallbackCheckBlock = [OCMArg checkWithBlock:^BOOL(dataTaskCallback callback) {
        urlSessionCallback = callback;
        return YES;
    }];
    
    id urlSessionCallbackInvocation = ^(NSInvocation *invocation) {
        urlSessionCallback(data, response, error);
    };
    
    [[[_urlSessionMock expect]
      andDo:urlSessionCallbackInvocation] dataTaskWithRequest:[OCMArg any] completionHandler:urlSessionCallbackCheckBlock];
}

@end
