//
//  InstalDL_UniversalLinkTest.m
//  InstalDL-TestBedTests
//
//  Created by Leonardo Passeri on 22/02/2018.
//  Copyright © 2018 Balzo. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import <InstalDL/InstalDL.h>
#import <InstalDL/IDLPreferenceHelper.h>
#import <InstalDL/IDLNetworkService.h>
#import <InstalDL/IDLServerResponse.h>
#import <InstalDL/IDLConstants.h>
#import <InstalDL/IDLError.h>

@interface IDLNetworkService (Test)
-(NSURLSession *)getUrlSession;
@end

@interface InstalDL_UniversalLinkTest : XCTestCase

@end

@implementation InstalDL_UniversalLinkTest {
    id _preferenceHelperMock;
    id _networkServiceMock;
    id _urlSessionMock;
    InstalDL * _instalDL;
}

- (void)setUp {
    [super setUp];
    
    _preferenceHelperMock = OCMPartialMock([IDLPreferenceHelper preferenceHelper]);
    OCMStub([_preferenceHelperMock firstInstall]).andReturn(YES);
    OCMStub([_preferenceHelperMock setFirstInstall:NO]);
    
    _urlSessionMock = OCMClassMock([NSURLSession class]);
    
    _networkServiceMock = OCMPartialMock([IDLNetworkService new]);
    OCMStub([_networkServiceMock getUrlSession]).andReturn(_urlSessionMock);
    
    _instalDL = [[InstalDL alloc] initWithInterface:_networkServiceMock preferenceHelper:_preferenceHelperMock];
}

- (void)tearDown {
    
    [_preferenceHelperMock stopMocking];
    [_networkServiceMock stopMocking];
    [_urlSessionMock stopMocking];
    
    [super tearDown];
}

- (void)testSuccessCallback
{
    NSHTTPURLResponse * response = [[NSHTTPURLResponse alloc] initWithURL:[NSURL URLWithString:@""] statusCode:200 HTTPVersion:nil headerFields:nil];
    NSData * data = [NSJSONSerialization dataWithJSONObject:@{IDL_RESPONSE_KEY_PARAMS : @{}} options:NSJSONWritingPrettyPrinted error:nil];
    
    [self expectUrlSessionResponse:response data:data error:nil];
    
    XCTestExpectation *expectation = [self expectationWithDescription:@"Deep link success"];
    
    [_instalDL initSessionWithLaunchOptions:nil andRegisterDeepLinkHandler:^(NSDictionary * _Nullable params, NSError * _Nullable error) {
        
        XCTAssertNil(error);
        XCTAssert(params);
        
        [expectation fulfill];
    }];
    
    NSUserActivity * userActivity = [[NSUserActivity alloc] initWithActivityType:@"com.instal.InstalDL-TestBed"];
    userActivity.webpageURL = [NSURL URLWithString:@"https://jgh3.rdrdl.instal.com/bellu"];
    XCTAssertTrue([_instalDL continueUserActivity:userActivity]);
    
    [self waitForExpectationsWithTimeout:10.0 handler:nil];
}

- (void)testFailureWrongUrlCallback
{
    id instalDlMock = OCMPartialMock(_instalDL);
    
    [instalDlMock initSessionWithLaunchOptions:nil andRegisterDeepLinkHandler:nil];
    
    NSUserActivity * userActivity = [[NSUserActivity alloc] initWithActivityType:@"com.instal.InstalDL-TestBed"];
    userActivity.webpageURL = [NSURL URLWithString:@"https://jgh3.rdrdl.installlll.com/bellu"];
    XCTAssertFalse([_instalDL continueUserActivity:userActivity]);
}

- (void)testFailureServerError
{
    NSHTTPURLResponse * response = [[NSHTTPURLResponse alloc] initWithURL:[NSURL URLWithString:@""] statusCode:500 HTTPVersion:nil headerFields:nil];
    
    [self expectUrlSessionResponse:response data:nil error:nil];
    
    XCTestExpectation *expectation = [self expectationWithDescription:@"Failure server error"];
    
    [_instalDL initSessionWithLaunchOptions:nil andRegisterDeepLinkHandler:^(NSDictionary * _Nullable params, NSError * _Nullable error) {
        
        XCTAssert(error);
        XCTAssertEqual(error.code, IDLServerProblemError);
        XCTAssertNil(params);
        
        [expectation fulfill];
    }];
    
    NSUserActivity * userActivity = [[NSUserActivity alloc] initWithActivityType:@"com.instal.InstalDL-TestBed"];
    userActivity.webpageURL = [NSURL URLWithString:@"https://jgh3.rdrdl.instal.com/bellu"];
    XCTAssertTrue([_instalDL continueUserActivity:userActivity]);
    
    [self waitForExpectationsWithTimeout:10.0 handler:nil];
}

-(void)expectUrlSessionResponse:(NSHTTPURLResponse *)response data:(NSData *)data error:(NSError *)error {
    
    typedef void (^dataTaskCallback) (NSData *data, NSURLResponse *urlResponse, NSError *error);
    
    __block dataTaskCallback urlSessionCallback;
    id urlSessionCallbackCheckBlock = [OCMArg checkWithBlock:^BOOL(dataTaskCallback callback) {
        urlSessionCallback = callback;
        return YES;
    }];
    
    id urlSessionCallbackInvocation = ^(NSInvocation *invocation) {
        urlSessionCallback(data, response, error);
        
    };
    
    [[[_urlSessionMock expect]
      andDo:urlSessionCallbackInvocation] dataTaskWithRequest:[OCMArg any] completionHandler:urlSessionCallbackCheckBlock];
}

@end
