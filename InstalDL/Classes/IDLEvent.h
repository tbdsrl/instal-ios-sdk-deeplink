//
//  IDLEvent.h
//  InstalDL-TestBed
//
//  Created by Leonardo Passeri on 19/02/2018.
//  Copyright © 2018 Balzo. All rights reserved.
//

#import "IDLConstants.h"

@class IDLNetworkService;
@class IDLPreferenceHelper;

@interface IDLEvent : NSObject

-(id)initWithEventType:(NSString *)eventType eventValue:(NSNumber *)eventValue;

-(void)sendEvent:(IDLNetworkService *)networkService preferenceHelper:(IDLPreferenceHelper *)preferenceHelper;

+(BOOL)isReservedEventType:(NSString *)eventType;

@end
