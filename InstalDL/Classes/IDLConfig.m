//
//  IDLConfig.m
//  InstalDL-TestBed
//
//  Created by Leonardo Passeri on 15/02/2018.
//  Copyright © 2018 Balzo. All rights reserved.
//

#import "IDLConfig.h"

NSString * const IDL_API_BASE_URL = @"https://rdrdl.instal.com";
NSString * const IDL_API_VERSION  = @"v1";
NSString * const IDL_SDK_VERSION  = @"0.1";
