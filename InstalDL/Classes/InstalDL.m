//
//  InstalDL.m
//  InstalDL
//
//  Created by Leonardo Passeri on 14/02/2018.
//  Copyright © 2018 Balzo. All rights reserved.
//

#import "InstalDL.h"
#import "IDLPreferenceHelper.h"
#import "IDLNetworkService.h"
#import "IDLServerRequest.h"
#import "IDLServerResponse.h"
#import "IDLConstants.h"
#import "IDLError.h"
#import "IDLEvent.h"
#import "IDLEncodingUtils.h"
#import "IDLLog.h"

@interface InstalDL ()

@property (copy,   nonatomic) callbackWithParams sessionInitWithParamsCallback;
@property (strong, nonatomic) IDLNetworkService *networkService;
@property (strong, nonatomic) IDLPreferenceHelper *preferenceHelper;

@end

@implementation InstalDL

#pragma mark - GetInstance methods

+ (InstalDL *)getInstance {
    static InstalDL *instance = nil;
    @synchronized (self) {
        static dispatch_once_t onceToken = 0;
        dispatch_once(&onceToken, ^{
            
            instance = [[InstalDL alloc] initWithInterface:[[IDLNetworkService alloc] init]
                             preferenceHelper:[IDLPreferenceHelper preferenceHelper]];
            
        });
        return instance;
    }
}

- (id)initWithInterface:(IDLNetworkService *)networkService
       preferenceHelper:(IDLPreferenceHelper *)preferenceHelper {
    
    self = [super init];
    if (!self) return self;
    
    _networkService = networkService;
    _networkService.preferenceHelper = preferenceHelper;
    _preferenceHelper = preferenceHelper;
    
    IDLLogInitialize();
    IDLLogSetDisplayLevel(IDLLogLevelWarning);
    
    return self;
}

#pragma mark - Init Session Methods

- (void)initSessionWithLaunchOptions:(NSDictionary *)options andRegisterDeepLinkHandler:(callbackWithParams)callback {
    self.sessionInitWithParamsCallback = callback;
    
    self.preferenceHelper.sendOpenEvent = [self maybeOpenThroughInstalDL:options];
    
    if (self.preferenceHelper.firstInstall) {
        
        IDLLogDebug(@"INSTALLATION FLOW");
        
        // Persist that from now on launches won't be first install
        self.preferenceHelper.firstInstall = NO;
        
        // INSTALLATION FLOW
        
        // -- Send Empty Request to 'deeplink' endpoint
        IDLServerRequest * request = [[IDLServerRequest alloc] initWithRequestType:IDLRequestDeeplink params:@{}];
        [self.networkService sendRequest:request callback:^(IDLServerResponse *response, NSError *error) {
            NSDictionary * routingParams = nil;
            NSError * idlError = nil;
            if (!error) {
                if (response.statusCode == 200) {
                    // -- If response is 200:
                    IDLLog(@"App installed through Instal deep link");
                    
                    routingParams = [response.data objectForKey:IDL_RESPONSE_KEY_PARAMS];
                    
                    // ---- Persist server data;
                    self.preferenceHelper.referrer = [response.data objectForKey:IDL_SHARED_KEY_REFERRER];
                    self.preferenceHelper.deeplinkClickId = [response.data objectForKey:IDL_SHARED_KEY_DEEPLINK_CLICK_ID];
                    self.preferenceHelper.deepLinkId = [response.data objectForKey:IDL_SHARED_KEY_DEEPLINK_ID];
                    self.preferenceHelper.identityCookie = [response.data objectForKey:IDL_SHARED_KEY_IDENTITY_COOKIE];
                    
                    // ---- Send Install Event;
                    IDLEvent * installEvent = [[IDLEvent alloc] initWithEventType:IDL_EVENT_NAME_INSTALL
                                                                       eventValue:nil];
                    [installEvent sendEvent:self.networkService preferenceHelper:self.preferenceHelper];
                } else if (response.statusCode == 404) {
                    IDLLog(@"App installed not through Instal deep link");
                } else {
                    idlError = [NSError idlErrorWithCode:IDLUnexpectedError];
                }
            } else {
                idlError = error;
            }
            
            if (idlError) {
                IDLLogError(@"INSTALLATION FLOW ERROR: %@", [idlError localizedDescription]);
            }
            
            // Call callback
            if (self.sessionInitWithParamsCallback) {
                self.sessionInitWithParamsCallback(routingParams, idlError);
            }
        }];
    }
}

- (BOOL)continueUserActivity:(NSUserActivity *)userActivity {
    
    return [self handleUniversalDeepLink:userActivity.webpageURL];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    
    return [self handleDeepLink:url];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
            options:(NSDictionary<NSString*,id> *)options {
    
    NSString *source = nil;
    NSString *annotation = nil;
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wpartial-availability"
    if (UIApplicationOpenURLOptionsSourceApplicationKey &&
        UIApplicationOpenURLOptionsAnnotationKey) {
        source = options[UIApplicationOpenURLOptionsSourceApplicationKey];
        annotation = options[UIApplicationOpenURLOptionsAnnotationKey];
    }
#pragma clang diagnostic pop
    return [self application:application openURL:url sourceApplication:source annotation:annotation];
}

#pragma mark - Handle Deep Link

- (BOOL)handleDeepLink:(NSURL *)url {
    NSString *scheme = [url scheme];
    if ([scheme isEqualToString:@"http"] || [scheme isEqualToString:@"https"]) {
        return [self handleUniversalDeepLink:url];
    } else {
        return [self handleSchemeDeepLink:url];
    }
}

- (BOOL)handleSchemeDeepLink:(NSURL*)url {
    BOOL handled = NO;
    
    // CUSTOM URI SCHEME OPEN FLOW
    
    // Check if Scheme is valid
    NSString * deeplinkClickId = [self getDeeplinkClickIdFromSchemaUrl:url];
    if (deeplinkClickId) {
        handled = YES;
        
        IDLLogDebug(@"URI SCHEME OPEN FLOW");
        
        // -- Send Request with deeplink click ID to 'deeplink' endpoint
        NSDictionary * params = @{IDL_SHARED_KEY_DEEPLINK_CLICK_ID : deeplinkClickId};
        IDLServerRequest * request = [[IDLServerRequest alloc] initWithRequestType:IDLRequestDetail params:params];
        [self.networkService sendRequest:request callback:^(IDLServerResponse *response, NSError *error) {
            NSDictionary * routingParams = nil;
            NSError * idlError = nil;
            if (!error) {
                if (response.statusCode == 200) {
                    // -- If response is 200:
                    IDLLog(@"App opened through URI schema deep link");
                    
                    routingParams = [response.data objectForKey:IDL_RESPONSE_KEY_PARAMS];
                    
                    // ---- Persist server data;
                    self.preferenceHelper.referrer = [response.data objectForKey:IDL_SHARED_KEY_REFERRER];
                    self.preferenceHelper.deeplinkClickId = [response.data objectForKey:IDL_SHARED_KEY_DEEPLINK_CLICK_ID];
                    self.preferenceHelper.deepLinkId = [response.data objectForKey:IDL_SHARED_KEY_DEEPLINK_ID];
                    self.preferenceHelper.identityCookie = [response.data objectForKey:IDL_SHARED_KEY_IDENTITY_COOKIE];
                    
                    // ---- Send Open / Reopen Event;
                    NSString * eventType = (self.preferenceHelper.sendOpenEvent) ? IDL_EVENT_NAME_OPEN : IDL_EVENT_NAME_REOPEN;
                    IDLEvent * openEvent = [[IDLEvent alloc] initWithEventType:eventType
                                                                       eventValue:nil];
                    [openEvent sendEvent:self.networkService preferenceHelper:self.preferenceHelper];
                } else {
                    idlError = [NSError idlErrorWithCode:IDLUnexpectedError];
                }
            } else {
                idlError = error;
            }
            
            if (idlError) {
                IDLLogError(@"URI SCHEME OPEN FLOW ERROR: %@", [idlError localizedDescription]);
            }
            // Call callback
            if (self.sessionInitWithParamsCallback) {
                self.sessionInitWithParamsCallback(routingParams, idlError);
            }
            self.preferenceHelper.sendOpenEvent = NO;
        }];
    } else {
        self.preferenceHelper.sendOpenEvent = NO;
    }
    
    return handled;
}

- (BOOL)handleUniversalDeepLink:(NSURL*)url {
    
    BOOL handled = NO;
    
    // UNIVERSAL LINK FLOW
    
    // Check if Universal Link is valid and belong to Instal
    if ([self isInstalDeeplink:url]) {
        handled = YES;
        
        IDLLogDebug(@"UNIVERSAL LINK FLOW");
        
        // -- Send Request with plain universal link url to 'click' endpoint
        NSDictionary * params = @{IDL_REQUEST_KEY_DEEPLINK : [url absoluteString]};
        IDLServerRequest * request = [[IDLServerRequest alloc] initWithRequestType:IDLRequestClick params:params];
        [self.networkService sendRequest:request callback:^(IDLServerResponse *response, NSError *error) {
            NSDictionary * routingParams = nil;
            NSError * idlError = nil;
            if (!error) {
                if (response.statusCode == 200) {
                    // -- If response is 200:
                    IDLLog(@"App opened through Instal universal deep link");
                    
                    routingParams = [response.data objectForKey:IDL_RESPONSE_KEY_PARAMS];
                    
                    // ---- Cache Deeplink click ID
                    self.preferenceHelper.deeplinkClickId = [response.data objectForKey:IDL_SHARED_KEY_DEEPLINK_CLICK_ID];
                    
                    // Call callback
                    if (self.sessionInitWithParamsCallback) {
                        self.sessionInitWithParamsCallback([response.data objectForKey:IDL_RESPONSE_KEY_PARAMS], idlError);
                    }
                    
                    // ---- Send Open / Reopen Event;
                    NSString * eventType = (self.preferenceHelper.sendOpenEvent) ? IDL_EVENT_NAME_OPEN : IDL_EVENT_NAME_REOPEN;
                    IDLEvent * openEvent = [[IDLEvent alloc] initWithEventType:eventType
                                                                    eventValue:nil];
                    [openEvent sendEvent:self.networkService preferenceHelper:self.preferenceHelper];
                } else {
                    idlError = [NSError idlErrorWithCode:IDLUnexpectedError];
                }
            } else {
                idlError = error;
            }
            
            if (idlError) {
                IDLLogError(@"UNIVERSAL LINK FLOW ERROR: %@", [idlError localizedDescription]);
            }
            // Call callback
            if (self.sessionInitWithParamsCallback) {
                self.sessionInitWithParamsCallback(routingParams, idlError);
            }
            
            self.preferenceHelper.sendOpenEvent = NO;
        }];
    } else {
        self.preferenceHelper.sendOpenEvent = NO;
    }
    
    return handled;
}

-(NSString *)getDeeplinkClickIdFromSchemaUrl:(NSURL *)url {
    NSString * deepLinkClickId = nil;
    if (url && ![url isEqual:[NSNull null]]) {
        
        NSString *query = [url fragment];
        if (!query) {
            query = [url query];
        }
        
        // ---- Cache Deeplink click ID
        NSDictionary *params = [IDLEncodingUtils decodeQueryStringToDictionary:query];
        deepLinkClickId = params[IDL_SCHEMA_KEY_DEEPLINK_CLICK_ID];
    }
    return deepLinkClickId;
}

-(BOOL)isInstalDeeplink:(NSURL *)url {
    NSString * urlString = [url absoluteString];
    id idlUniversalLinkDomains = [self.preferenceHelper getInstalDLUniversalLinkDomains];
    if ([idlUniversalLinkDomains isKindOfClass:[NSString class]] &&
        [urlString containsString:idlUniversalLinkDomains]) {
        return YES;
    }
    else if ([idlUniversalLinkDomains isKindOfClass:[NSArray class]]) {
        for (id oneDomain in idlUniversalLinkDomains) {
            if ([oneDomain isKindOfClass:[NSString class]] && [urlString containsString:oneDomain]) {
                return YES;
            }
        }
    }
    
    NSString *userActivityURL = urlString;
    NSArray *instalDomains = [NSArray arrayWithObjects:@"instal.com", nil];
    for (NSString* domain in instalDomains) {
        if ([userActivityURL containsString:domain])
            return YES;
    }
    return NO;
}

-(BOOL)maybeOpenThroughInstalDL:(NSDictionary *)launchOptions {
    return ([launchOptions.allKeys containsObject:UIApplicationLaunchOptionsURLKey] ||
            [launchOptions.allKeys containsObject:UIApplicationLaunchOptionsUserActivityDictionaryKey]);
}

#pragma mark - Event methods

- (void)sendEventType:(NSString *)eventType withValue:(NSNumber *)eventValue {
    
    if (![IDLEvent isReservedEventType:eventType]) {
        IDLEvent * customEvent = [[IDLEvent alloc] initWithEventType:eventType
                                                          eventValue:eventValue];
        [customEvent sendEvent:self.networkService preferenceHelper:self.preferenceHelper];
    } else {
        IDLLogError(@"Cannot send custom event: '%@' is a reserved event type", eventType);
    }
}

#pragma mark - Configuration methods

- (void)setDebug {
    if (IDLLogDisplayLevel() > IDLLogLevelDebug)
        IDLLogSetDisplayLevel(IDLLogLevelDebug);
}

@end
