//
//  IDLConfig.h
//  InstalDL-TestBed
//
//  Created by Leonardo Passeri on 15/02/2018.
//  Copyright © 2018 Balzo. All rights reserved.
//

#if __has_feature(modules)
@import Foundation;
#else
#import <Foundation/Foundation.h>
#endif

FOUNDATION_EXPORT NSString*_Nonnull const IDL_API_BASE_URL;
FOUNDATION_EXPORT NSString*_Nonnull const IDL_API_VERSION;
FOUNDATION_EXPORT NSString*_Nonnull const IDL_SDK_VERSION;

FOUNDATION_EXPORT NSString*_Nonnull const IDL_JWT_PRIVATE_KEY;
FOUNDATION_EXPORT NSString*_Nonnull const IDL_JWT_PUBLIC_KEY;


