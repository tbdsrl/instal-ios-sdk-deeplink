//
//  IDLPreferenceHelper.h
//  InstalDL
//
//  Created by Leonardo Passeri on 14/02/2018.
//  Copyright © 2018 Balzo. All rights reserved.
//

#import <Foundation/Foundation.h>

NSURL* IDLURLForIDLDirectory(void);

@interface IDLPreferenceHelper : NSObject

@property (assign, nonatomic) BOOL firstInstall;
@property (strong, nonatomic) NSString *deeplinkClickId;
@property (strong, nonatomic) NSString *deepLinkId;
@property (strong, nonatomic) NSString *referrer;
@property (strong, nonatomic) NSString *identityCookie;
@property (strong, nonatomic, readonly) NSString *userSdkID;
@property (strong, nonatomic, readonly) NSString *idfa;

@property (strong, atomic) NSString *idlAPIURL;

@property (assign, nonatomic) BOOL sendOpenEvent;

+ (IDLPreferenceHelper *)preferenceHelper;

- (NSString *)getAPIBaseURL;
- (NSString *)getAPIURL:(NSString *)endpoint;
- (NSString *)getEndpointFromURL:(NSString *)url;

- (id)getInstalDLUniversalLinkDomains;
- (NSString *)getJWTPrivateKey;
- (NSString *)getJWTPublicKey;

- (void) synchronize;  //  Flushes preference queue to persistence.

@end
