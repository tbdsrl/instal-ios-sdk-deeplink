//
//  InstalDL.h
//  InstalDL
//
//  Created by Leonardo Passeri on 14/02/2018.
//  Copyright © 2018 Balzo. All rights reserved.
//

#if __has_feature(modules)
@import Foundation;
@import UIKit;
#else
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#endif

#import "IDLCallbacks.h"

@class IDLNetworkService;
@class IDLPreferenceHelper;

@interface InstalDL : NSObject

#pragma mark Global Instance Accessors

///--------------------------------
/// @name Global Instance Accessors
///--------------------------------

/**
 Gets the global, live InstalDL instance.
 */
+ (InstalDL * _Nonnull)getInstance;

#pragma mark - Initialization methods

///---------------------
/// @name Initialization
///---------------------

/**
 Initialize the InstalDL session with the app launch options and handle the completion with a callback
 
 @param options The launch options provided by the AppDelegate's `didFinishLaunchingWithOptions:` method.
 @param callback A callback that is called when the session is opened. This will be called multiple times during the apps life, including any time the app goes through a background / foreground cycle.
 */
- (void)initSessionWithLaunchOptions:(NSDictionary * _Nullable)options andRegisterDeepLinkHandler:(callbackWithParams _Nullable)callback;

/**
 Allow InstalDL to handle restoration from an NSUserActivity, returning whether or not it was
 from a InstalDL link.
 
 @param userActivity The NSUserActivity that caused the app to be opened.
 */

- (BOOL)continueUserActivity:(NSUserActivity * _Nonnull)userActivity;

/**
 Call this method from inside your app delegate's `application:openURL:sourceApplication:annotation:`
 method with the so that InstalDL can open the passed URL.
 
 @param application         The application that was passed to your app delegate.
 @param url                 The URL that was passed to your app delegate.
 @param sourceApplication   The sourceApplication that was passed to your app delegate.
 @param annotation          The annotation that was passed to your app delegate.
 @return                    Returns `YES` if InstalDL handled the passed URL.
 */
- (BOOL)application:(UIApplication * _Nonnull)application
            openURL:(NSURL * _Nonnull)url
  sourceApplication:(NSString * _Nullable)sourceApplication
         annotation:(id _Nonnull)annotation;

/**
 Call this method from inside your app delegate's `application:openURL:options:`
 method with the so that InstalDL can open the passed URL.
 
 This method is functionally the same as calling the InstalDL method
 `application:openURL:sourceApplication:annotation:`. This method matches the new Apple appDelegate
 method for convenience.
 
 @param application         The application that was passed to your app delegate.
 @param url                 The URL that was passed to your app delegate.
 @param options             The options dictionary that was passed to your app delegate.
 @return                    Returns `YES` if InstalDL handled the passed URL.
 */
- (BOOL)application:(UIApplication * _Nonnull)application
            openURL:(NSURL * _Nonnull)url
            options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> * _Nullable)options;

#pragma mark - Event methods

/**
 Log a custom event to the Instal server
 
 @param eventType           Type of the event
 @param eventValue          Value of the event
 */
- (void)sendEventType:(NSString * _Nonnull)eventType withValue:(NSNumber * _Nullable)eventValue;

#pragma mark - Configuration methods

///--------------------
/// @name Configuration
///--------------------

/**
 Have InstalDL treat this device / session as a debug session, causing more information to be logged
 
 @warning This should not be used in production.
 */
- (void)setDebug;

#pragma mark - Test methods

/**
 Method for creating a one of InstalDL instance and specifying its dependencies.
 
 @warning This is meant for use internally only (exposed for the sake of testing) and should not be used by apps.
 */

- (id _Nonnull)initWithInterface:(IDLNetworkService * _Nonnull)networkService
       preferenceHelper:(IDLPreferenceHelper * _Nonnull)preferenceHelper;

@end
