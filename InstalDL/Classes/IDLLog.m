//
//  IDLLog.m
//  InstalDL-TestBed
//
//  Created by Leonardo Passeri on 22/02/2018.
//  Copyright © 2018 Balzo. All rights reserved.
//

#import "IDLLog.h"
#import <stdatomic.h> // import not available in Xcode 7

#define _countof(array)  (sizeof(array)/sizeof(array[0]))
static NSNumber *idl_LogIsInitialized = nil;

// All log synchronization and globals are coordinated through the idl_LogQueue.
static dispatch_queue_t idl_LogQueue = nil;

static IDLLogOutputFunctionPtr idl_LoggingFunction = nil; // Default to just NSLog output.
static NSDateFormatter *idl_LogDateFormatter = nil;

inline static void IDLLogInitializeClient_Internal() {
    IDLLogClientInitializeFunctionPtr initFunction = IDLLogSetClientInitializeFunction(NULL);
    if (initFunction) {
        initFunction();
    }
}

#pragma mark - Log Message Severity

static IDLLogLevel idl_LogDisplayLevel = IDLLogLevelWarning;

IDLLogLevel IDLLogDisplayLevel() {
    __block IDLLogLevel level = IDLLogLevelDebug;
    dispatch_sync(idl_LogQueue, ^{
        level = idl_LogDisplayLevel;
    });
    return level;
}

void IDLLogSetDisplayLevel(IDLLogLevel level) {
    IDLLogInitializeClient_Internal();
    dispatch_async(idl_LogQueue, ^{
        idl_LogDisplayLevel = level;
    });
}

static NSString*const idl_logLevelStrings[] = {
    @"IDLLogLevelDebug",
    @"IDLLogLevelWarning",
    @"IDLLogLevelError",
    @"IDLLogLevelLog",
    @"IDLLogLevelNone",
    @"IDLLogLevelMax"
};

NSString* IDLLogStringFromLogLevel(IDLLogLevel level) {
    level = MAX(MIN(level, IDLLogLevelMax), 0);
    return idl_logLevelStrings[level];
}

IDLLogLevel IDLLogLevelFromString(NSString*string) {
    if (!string) return IDLLogLevelNone;
    for (NSUInteger i = 0; i < _countof(idl_logLevelStrings); ++i) {
        if ([idl_logLevelStrings[i] isEqualToString:string]) {
            return i;
        }
    }
    if ([string isEqualToString:@"IDLLogLevelDebug"]) {
        return IDLLogLevelDebug;
    }
    return IDLLogLevelNone;
}

#pragma mark - Client Initialization Function

static _Atomic(IDLLogClientInitializeFunctionPtr) idl_LogClientInitializeFunctionPtr = (IDLLogClientInitializeFunctionPtr) 0;

extern IDLLogClientInitializeFunctionPtr _Null_unspecified IDLLogSetClientInitializeFunction(
                                                                                             IDLLogClientInitializeFunctionPtr _Nullable clientInitializationFunction
                                                                                             ) {
    IDLLogClientInitializeFunctionPtr lastPtr =
    atomic_exchange(&idl_LogClientInitializeFunctionPtr, clientInitializationFunction);
    return lastPtr;
}

#pragma mark - IDLLogInternal

void IDLLogWriteMessageFormat(
                              IDLLogLevel logLevel,
                              const char *_Nullable file,
                              int lineNumber,
                              NSString *_Nullable message,
                              ...
                              ) {
    IDLLogInitializeClient_Internal();
    if (!file) file = "";
    if (!message) message = @"<nil>";
    if (![message isKindOfClass:[NSString class]]) {
        message = [NSString stringWithFormat:@"0x%016llx <%@> %@",
                   (uint64_t) message, message.class, message.description];
    }
    
    NSString* filename =
    [[NSString stringWithCString:file encoding:NSMacOSRomanStringEncoding]
     lastPathComponent];
    
    NSString * const logLevels[IDLLogLevelMax] = {
        @"Debug",
        @"Warning",
        @"Error",
        @"Log",
        @"None",
    };
    
    logLevel = MAX(MIN(logLevel, IDLLogLevelMax-1), 0);
    NSString *levelString = logLevels[logLevel];
    
    va_list args;
    va_start(args, message);
    NSString* m = [[NSString alloc] initWithFormat:message arguments:args];
    NSString* s = [NSString stringWithFormat:
                   @"[instal.com] %@(%d) %@: %@", filename, lineNumber, levelString, m];
    va_end(args);
    
    dispatch_async(idl_LogQueue, ^{
        if (logLevel >= idl_LogDisplayLevel) {
            NSLog(@"%@", s); // Upgrade this to unified logging when we can.
        }
        if (idl_LoggingFunction)
            idl_LoggingFunction([NSDate date], logLevel, s);
    });
}

void IDLLogWriteMessage(
                        IDLLogLevel logLevel,
                        NSString *_Nonnull file,
                        NSUInteger lineNumber,
                        NSString *_Nonnull message
                        ) {
    IDLLogWriteMessageFormat(logLevel, file.UTF8String, (int)lineNumber, @"%@", message);
}

#pragma mark - IDLLogInitialize

__attribute__((constructor)) void IDLLogInitialize(void) {
    static dispatch_once_t onceToken = 0;
    dispatch_once(&onceToken, ^ {
        idl_LogQueue = dispatch_queue_create("io.instal.sdk.log", DISPATCH_QUEUE_SERIAL);
        
        idl_LogDateFormatter = [[NSDateFormatter alloc] init];
        idl_LogDateFormatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
        idl_LogDateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSSSSSX";
        idl_LogDateFormatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
        
        idl_LogIsInitialized = @(YES);
    });
}
