//
//  IDLConstants.m
//  InstalDL-TestBed
//
//  Created by Leonardo Passeri on 14/02/2018.
//  Copyright © 2018 Balzo. All rights reserved.
//

#import "IDLConstants.h"

NSString * const IDL_ENDPOINT = @"rdrdl.instal.com";

NSString * const IDL_EVENT_NAME_INSTALL = @"install";
NSString * const IDL_EVENT_NAME_OPEN = @"open";
NSString * const IDL_EVENT_NAME_REOPEN = @"reopen";

NSString * const IDL_SHARED_KEY_REFERRER = @"referrer";
NSString * const IDL_SHARED_KEY_DEEPLINK_CLICK_ID = @"dl_click_id";
NSString * const IDL_SHARED_KEY_DEEPLINK_ID = @"deeplink_id";
NSString * const IDL_SHARED_KEY_IDENTITY_COOKIE = @"identity_cookie";

NSString * const IDL_REQUEST_KEY_USER_SDK_ID = @"user_sdk_id";
NSString * const IDL_REQUEST_KEY_IDFA = @"idfa";
NSString * const IDL_REQUEST_KEY_EVENT_TYPE = @"event_type";
NSString * const IDL_REQUEST_KEY_EVENT_VALUE = @"event_value";
NSString * const IDL_REQUEST_KEY_DEEPLINK = @"deeplink";
NSString * const IDL_REQUEST_KEY_DATA = @"data";

NSString * const IDL_RESPONSE_KEY_PARAMS = @"params";
NSString * const IDL_RESPONSE_KEY_DESCRIPTION = @"description";

NSString * const IDL_SCHEMA_KEY_DEEPLINK_CLICK_ID = @"dl_click_id";

NSString * const IDL_QUERY_PARAM_PUBLIC_KEY = @"public_key";
