//
//  IDLEvent.m
//  InstalDL-TestBed
//
//  Created by Leonardo Passeri on 19/02/2018.
//  Copyright © 2018 Balzo. All rights reserved.
//

#import "IDLEvent.h"
#import "IDLNetworkService.h"
#import "IDLPreferenceHelper.h"
#import "IDLServerRequest.h"
#import "IDLServerResponse.h"
#import "IDLError.h"
#import "IDLConstants.h"
#import "IDLLog.h"

@interface IDLEvent()

@property (nonatomic, assign) NSString * eventType;
@property (nonatomic, strong) NSNumber * eventValue;

@end

@implementation IDLEvent

-(id)initWithEventType:(NSString *)eventType eventValue:(NSNumber *)eventValue {
    if (self = [super init]) {
        _eventType = eventType;
        _eventValue = eventValue;
    }
    return self;
}

-(void)sendEvent:(IDLNetworkService *)networkService preferenceHelper:(IDLPreferenceHelper *)preferenceHelper {
    
    NSMutableDictionary * requestParams = [NSMutableDictionary dictionary];
    
    // Set event type and event value
    [self safeSetValue:_eventType forKey:IDL_REQUEST_KEY_EVENT_TYPE onDict:requestParams];
    [self safeSetValue:_eventValue forKey:IDL_REQUEST_KEY_EVENT_VALUE onDict:requestParams];
    
    // Decore event params with statistics data
    [self safeSetValue:preferenceHelper.referrer forKey:IDL_SHARED_KEY_REFERRER onDict:requestParams];
    [self safeSetValue:preferenceHelper.deepLinkId forKey:IDL_SHARED_KEY_DEEPLINK_ID onDict:requestParams];
    [self safeSetValue:preferenceHelper.deeplinkClickId forKey:IDL_SHARED_KEY_DEEPLINK_CLICK_ID onDict:requestParams];
    [self safeSetValue:preferenceHelper.identityCookie forKey:IDL_SHARED_KEY_IDENTITY_COOKIE onDict:requestParams];
    [self safeSetValue:preferenceHelper.userSdkID forKey:IDL_REQUEST_KEY_USER_SDK_ID onDict:requestParams];
    [self safeSetValue:preferenceHelper.idfa forKey:IDL_REQUEST_KEY_IDFA onDict:requestParams];
    
    IDLServerRequest * request = [[IDLServerRequest alloc] initWithRequestType:IDLRequestEvents
                                                                        params:requestParams];
    NSString * eventType = _eventType;
    [networkService sendRequest:request callback:^(IDLServerResponse *response, NSError *error) {
        if (!error) {
            if (response.statusCode == 200) {
                IDLLog(@"EVENT TRANSMISSION PROTOCOL INFO: event '%@' sent successfully", eventType);
            } else {
                IDLLogError(@"EVENT TRANSMISSION PROTOCOL ERROR: %@", [[NSError idlErrorWithCode:IDLUnexpectedError] localizedDescription]);
            }
        } else {
            IDLLogError(@"EVENT TRANSMISSION PROTOCOL ERROR: %@", [error localizedDescription]);
        }
    }];
}

- (void)safeSetValue:(NSObject *)value forKey:(NSString *)key onDict:(NSMutableDictionary *)dict {
    if (value) {
        dict[key] = value;
    }
}

+(BOOL)isReservedEventType:(NSString *)eventType {
    return ([eventType isEqualToString:IDL_EVENT_NAME_INSTALL] ||
            [eventType isEqualToString:IDL_EVENT_NAME_OPEN] ||
            [eventType isEqualToString:IDL_EVENT_NAME_REOPEN]);
}

@end
