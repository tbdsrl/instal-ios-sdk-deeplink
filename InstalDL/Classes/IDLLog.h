//
//  IDLLog.h
//  InstalDL-TestBed
//
//  Created by Leonardo Passeri on 22/02/2018.
//  Copyright © 2018 Balzo. All rights reserved.
//

#if __has_feature(modules)
@import Foundation;
#else
#import <Foundation/Foundation.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif
    
    ///@functiongroup InstalDL Logging Functions
    
#pragma mark Log Initialization
    
    /// Log facility initialization. Usually there is no need to call this directly.
    extern void IDLLogInitialize(void) __attribute__((constructor));
    
#pragma mark Log Message Severity
    
    /// Log message severity
    typedef NS_ENUM(NSInteger, IDLLogLevel) {
        IDLLogLevelDebug = 0,
        IDLLogLevelWarning,
        IDLLogLevelError,
        IDLLogLevelLog,
        IDLLogLevelNone,
        IDLLogLevelMax
    };
    
    /*!
     * @return Returns the current log severity display level.
     */
    extern IDLLogLevel IDLLogDisplayLevel(void);
    
    /*!
     * @param level Sets the current display level for log messages.
     */
    extern void IDLLogSetDisplayLevel(IDLLogLevel level);
    
#pragma mark - Client Initialization Function
    
    typedef void (*IDLLogClientInitializeFunctionPtr)(void);
    
    ///@param clientInitializationFunction The client function that should be called before logging starts.
    extern IDLLogClientInitializeFunctionPtr _Null_unspecified
    IDLLogSetClientInitializeFunction(IDLLogClientInitializeFunctionPtr _Nullable clientInitializationFunction);
    
#pragma mark - Optional Log Output Handlers
    
    ///@brief Pre-defined log message handlers --
    
    typedef void (*IDLLogOutputFunctionPtr)(NSDate*_Nonnull timestamp, IDLLogLevel level, NSString*_Nullable message);
    
#pragma mark - IDLLogWriteMessage
    
    
    /// The main logging function used in the variadic logging defines.
    extern void IDLLogWriteMessageFormat(
                                         IDLLogLevel logLevel,
                                         const char *_Nullable sourceFileName,
                                         int sourceLineNumber,
                                         id _Nullable messageFormat,
                                         ...
                                         );
    
    /// Swift-friendly wrapper for IDLLogWriteMessageFormat
    extern void IDLLogWriteMessage(
                                   IDLLogLevel logLevel,
                                   NSString *_Nonnull sourceFileName,
                                   NSUInteger sourceLineNumber,
                                   NSString *_Nonnull message
                                   );
    
#pragma mark - Logging
    ///@info Logging
    
    ///@param format Log a debug message with the specified formatting.
#define IDLLogDebug(...) \
do  { IDLLogWriteMessageFormat(IDLLogLevelDebug, __FILE__, __LINE__, __VA_ARGS__); } while (0)
    
    ///@param format Log a warning message with the specified formatting.
#define IDLLogWarning(...) \
do  { IDLLogWriteMessageFormat(IDLLogLevelWarning, __FILE__, __LINE__, __VA_ARGS__); } while (0)
    
    ///@param format Log an error message with the specified formatting.
#define IDLLogError(...) \
do  { IDLLogWriteMessageFormat(IDLLogLevelError, __FILE__, __LINE__, __VA_ARGS__); } while (0)
    
    ///@param format Log a message with the specified formatting.
#define IDLLog(...) \
do  { IDLLogWriteMessageFormat(IDLLogLevelLog, __FILE__, __LINE__, __VA_ARGS__); } while (0)
    
    ///Write the name of the current method to the log.
#define IDLLogMethodName() \
IDLLogDebug(@"Method '%@'.",  NSStringFromSelector(_cmd))
    
    ///Write the name of the current function to the log.
#define IDLLogFunctionName() \
IDLLogDebug(@"Function '%s'.", __FUNCTION__)
    
#ifdef __cplusplus
}
#endif
