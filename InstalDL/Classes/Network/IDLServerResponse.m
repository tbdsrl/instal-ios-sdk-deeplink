//
//  IDLServerResponse.m
//  InstalDL
//
//  Created by Leonardo Passeri on 14/02/2018.
//  Copyright © 2018 Balzo. All rights reserved.
//

#import "IDLServerResponse.h"

@implementation IDLServerResponse

- (NSString *)description {
    return [NSString stringWithFormat:@"Status: %ld; Data: %@", self.statusCode, self.data];
}

@end
