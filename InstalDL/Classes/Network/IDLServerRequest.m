//
//  IDLServerRequest.m
//  InstalDL
//
//  Created by Leonardo Passeri on 14/02/2018.
//  Copyright © 2018 Balzo. All rights reserved.
//

#import "IDLServerRequest.h"

@implementation IDLServerRequest

-(id)initWithRequestType:(IDLRequestType)requestType params:(NSDictionary *)params {
    if (self = [super init]) {
        _requestType = requestType;
        _params = params;
    }
    return self;
}

@end
