//
//  IDLNetworkService.m
//  InstalDL
//
//  Created by Leonardo Passeri on 14/02/2018.
//  Copyright © 2018 Balzo. All rights reserved.
//

#import "IDLNetworkService.h"
#import "IDLServerRequest.h"
#import "IDLServerResponse.h"
#import "IDLPreferenceHelper.h"
#import "IDLError.h"
#import "IDLEncodingUtils.h"
#import "IDLConstants.h"
#import "IDLLog.h"

#if TARGET_OS_IOS || TARGET_OS_TV
#import <UIKit/UIKit.h>
#elif TARGET_OS_WATCH
#import <WatchKit/WatchKit.h>
#endif

static NSString *const IDL_REQUEST_API_DEEPLINK = @"tracking/deeplinks";
static NSString *const IDL_REQUEST_API_DETAIL = @"tracking/details";
static NSString *const IDL_REQUEST_API_CLICK = @"tracking/click";
static NSString *const IDL_REQUEST_API_EVENTS = @"tracking/events";

static NSString *const IDL_CONTENT_TYPE = @"application/x-www-form-urlencoded";

@interface IDLNetworkService ()

@end

@implementation IDLNetworkService

-(void)sendRequest:(IDLServerRequest *)request callback:(IDLServerCallback)callback {
    NSError *error = nil;
    NSURLRequest * urlRequest = [self urlRequestFromIDLRequest:request error:&error];
    
    if (!error) {
        NSURLSession *session = [self getUrlSession];
        NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *urlResponse, NSError *error) {
            
            IDLServerResponse * response = nil;
            NSError * idlError = nil;
            
            IDLLogDebug(@"Response: %@\nData: %@\nError: %@", urlResponse, data, error);
            
            if (!error) {
                response = [[IDLServerResponse alloc] init];
                response.statusCode = ((NSHTTPURLResponse *)urlResponse).statusCode;
                
                if (response.statusCode >= 500) {
                    idlError = [NSError idlErrorWithCode:IDLServerProblemError];
                } else if ((response.statusCode == 400 || response.statusCode == 200) && data && data.length > 0) {
                    NSError *jsonError;
                    NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&jsonError];
                    if (jsonError == nil) {
                        if (response.statusCode == 200) {
                            response.data = responseDict;
                        } else if (response.statusCode == 400) {
                            NSString * errorDescription = [responseDict objectForKey:IDL_RESPONSE_KEY_DESCRIPTION];
                            idlError = [NSError idlErrorWithCode:IDLBadRequestError localizedMessage:errorDescription];
                        }
                    } else {
                        idlError = [NSError idlErrorWithCode:IDLResponseParseError error:jsonError];
                    }
                }
                IDLLogDebug(@"Response Data: %@\n\nError: %@", response.data, idlError);
                
            } else {
                idlError = [NSError idlErrorWithCode:IDLConnectivityError error:error];
            }
            if (callback) {
                if ([NSThread isMainThread]) {
                    callback(response, idlError);
                } else {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        callback(response, idlError);
                    });
                }
            }
            
        }];
        [postDataTask resume];
    } else {
        if (callback) {
            callback(nil, error);
        }
    }
}

-(NSURLSession *)getUrlSession {
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    return [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
}

-(NSString *)urlStringByRequestType:(IDLRequestType)type {
    
    NSString * urlString = @"";
    switch (type)
    {
        case IDLRequestDeeplink:
            urlString = [self.preferenceHelper getAPIURL:IDL_REQUEST_API_DEEPLINK];
            break;
        case IDLRequestDetail:
            urlString = [self.preferenceHelper getAPIURL:IDL_REQUEST_API_DETAIL];
            break;
        case IDLRequestClick:
            urlString = [self.preferenceHelper getAPIURL:IDL_REQUEST_API_CLICK];
            break;
        case IDLRequestEvents:
            urlString = [self.preferenceHelper getAPIURL:IDL_REQUEST_API_EVENTS];
            break;
        default:
            break;
    }
    
    return urlString;
}

-(NSURLRequest *)urlRequestFromIDLRequest:(IDLServerRequest *)request error:(NSError * _Nullable *)errorPtr {
    
    // Retreive private/public keys for JWT signing protocol
    NSString * publicKey = [self.preferenceHelper getJWTPublicKey];
    if (!publicKey) {
        *errorPtr = [NSError idlErrorWithCode:IDLMissingPublicKeyError];
        return nil;
    }
    NSString * privateKey = [self.preferenceHelper getJWTPrivateKey];
    if (!privateKey) {
        *errorPtr = [NSError idlErrorWithCode:IDLMissingPrivateKeyError];
        return  nil;
    }
    
    NSMutableURLRequest *urlRequest = nil;
    
    // Get endpoint from request type
    NSString * requestUrlString = [self urlStringByRequestType:request.requestType];
    
    // Append public key to url
    NSString * queryStr = [IDLEncodingUtils encodeDictionaryToQueryString:@{IDL_QUERY_PARAM_PUBLIC_KEY : publicKey}];
    requestUrlString = [requestUrlString stringByAppendingString:queryStr];
    
    // Encode request params through JWT
    NSString * encodedPayload = [IDLEncodingUtils encodeDictionaryToJWT:request.params withKey:privateKey];
    
    // Set dictionary {"data": <Encoded request params>}
    NSDictionary * params = @{IDL_REQUEST_KEY_DATA : encodedPayload};
    
    NSError * jsonError = nil;
    
    // Encode dictionary in JSON Object (payload)
    NSData * dataPayload = [NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:&jsonError];
    
    if (!jsonError) {
        urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestUrlString]
                                             cachePolicy:NSURLRequestUseProtocolCachePolicy
                                         timeoutInterval:60.0];
        
        [self addDefaultHeadersToUrlRequest:urlRequest];
        [urlRequest setHTTPMethod:@"POST"];
        [urlRequest setHTTPBody:dataPayload];
        
        // TEMP dirty test to simulate first installation
//        [urlRequest setValue:@"0.0.0.11" forHTTPHeaderField:@"X-Forwarded-For"];
        
        IDLLogDebug(@"Request:\nUrl: '%@'\nHeaders:%@\nPlain Params: %@", [urlRequest URL], [urlRequest allHTTPHeaderFields], [request params]);
        
    } else if (errorPtr) {
        *errorPtr = [NSError idlErrorWithCode:IDLRequestEncodeError];
    }
    
    return urlRequest;
}

-(void)addDefaultHeadersToUrlRequest:(NSMutableURLRequest *)urlRequest {
    
    [urlRequest addValue:IDL_CONTENT_TYPE forHTTPHeaderField:@"Content-Type"];
    
    // Accept-Language HTTP Header; see http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.4
    NSMutableArray *acceptLanguagesComponents = [NSMutableArray array];
    [[NSLocale preferredLanguages] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        float q = 1.0f - (idx * 0.1f);
        [acceptLanguagesComponents addObject:[NSString stringWithFormat:@"%@;q=%0.1g", obj, q]];
        *stop = q <= 0.5f;
    }];
    [urlRequest addValue:[acceptLanguagesComponents componentsJoinedByString:@", "] forHTTPHeaderField:@"Accept-Language"];
    
    NSString *userAgent = nil;
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wgnu"
#if TARGET_OS_IOS
    // User-Agent Header; see http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.43
    userAgent = [NSString stringWithFormat:@"%@/%@ (%@; iOS %@; Scale/%0.2f)", [[NSBundle mainBundle] infoDictionary][(__bridge NSString *)kCFBundleExecutableKey] ?: [[NSBundle mainBundle] infoDictionary][(__bridge NSString *)kCFBundleIdentifierKey], [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"] ?: [[NSBundle mainBundle] infoDictionary][(__bridge NSString *)kCFBundleVersionKey], [[UIDevice currentDevice] model], [[UIDevice currentDevice] systemVersion], [[UIScreen mainScreen] scale]];
#elif TARGET_OS_WATCH
    // User-Agent Header; see http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.43
    userAgent = [NSString stringWithFormat:@"%@/%@ (%@; watchOS %@; Scale/%0.2f)", [[NSBundle mainBundle] infoDictionary][(__bridge NSString *)kCFBundleExecutableKey] ?: [[NSBundle mainBundle] infoDictionary][(__bridge NSString *)kCFBundleIdentifierKey], [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"] ?: [[NSBundle mainBundle] infoDictionary][(__bridge NSString *)kCFBundleVersionKey], [[WKInterfaceDevice currentDevice] model], [[WKInterfaceDevice currentDevice] systemVersion], [[WKInterfaceDevice currentDevice] screenScale]];
#elif defined(__MAC_OS_X_VERSION_MIN_REQUIRED)
    userAgent = [NSString stringWithFormat:@"%@/%@ (Mac OS X %@)", [[NSBundle mainBundle] infoDictionary][(__bridge NSString *)kCFBundleExecutableKey] ?: [[NSBundle mainBundle] infoDictionary][(__bridge NSString *)kCFBundleIdentifierKey], [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"] ?: [[NSBundle mainBundle] infoDictionary][(__bridge NSString *)kCFBundleVersionKey], [[NSProcessInfo processInfo] operatingSystemVersionString]];
#endif
#pragma clang diagnostic pop
    if (userAgent) {
        if (![userAgent canBeConvertedToEncoding:NSASCIIStringEncoding]) {
            NSMutableString *mutableUserAgent = [userAgent mutableCopy];
            if (CFStringTransform((__bridge CFMutableStringRef)(mutableUserAgent), NULL, (__bridge CFStringRef)@"Any-Latin; Latin-ASCII; [:^ASCII:] Remove", false)) {
                userAgent = mutableUserAgent;
            }
        }
        [urlRequest addValue:userAgent forHTTPHeaderField:@"User-Agent"];
    }
}

@end
