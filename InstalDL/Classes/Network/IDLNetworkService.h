//
//  IDLNetworkService.h
//  InstalDL
//
//  Created by Leonardo Passeri on 14/02/2018.
//  Copyright © 2018 Balzo. All rights reserved.
//

#if __has_feature(modules)
@import Foundation;
#else
#import <Foundation/Foundation.h>
#endif

@class IDLPreferenceHelper;
@class IDLServerRequest;
@class IDLServerResponse;

typedef void (^IDLServerCallback)(IDLServerResponse *response, NSError *error);

@interface IDLNetworkService : NSObject

@property (nonatomic, strong) IDLPreferenceHelper *preferenceHelper;

-(void)sendRequest:(IDLServerRequest *)request callback:(IDLServerCallback)callback;

@end
