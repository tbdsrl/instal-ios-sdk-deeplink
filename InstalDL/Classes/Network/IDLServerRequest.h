//
//  IDLServerRequest.h
//  InstalDL
//
//  Created by Leonardo Passeri on 14/02/2018.
//  Copyright © 2018 Balzo. All rights reserved.
//

#if __has_feature(modules)
@import Foundation;
#else
#import <Foundation/Foundation.h>
#endif

typedef NS_ENUM(NSInteger, IDLRequestType) {
    IDLRequestDeeplink,
    IDLRequestDetail,
    IDLRequestClick,
    IDLRequestEvents,
};

@interface IDLServerRequest : NSObject

@property (nonatomic, assign, readonly) IDLRequestType requestType;
@property (nonatomic, strong, readonly) NSDictionary * params;

-(id)initWithRequestType:(IDLRequestType)requestType params:(NSDictionary *)params;

@end
