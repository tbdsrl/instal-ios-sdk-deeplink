//
//  IDLPreferenceHelper.m
//  InstalDL
//
//  Created by Leonardo Passeri on 14/02/2018.
//  Copyright © 2018 Balzo. All rights reserved.
//

#import "IDLPreferenceHelper.h"
#import "IDLConfig.h"
#import "IDLLog.h"
#import <AdSupport/ASIdentifierManager.h>

static NSString * const IDL_PREFS_FILE = @"IDLPreferences";

static NSString * const IDL_PREFS_KEY_FIRST_INSTALL = @"idl_first_install";
static NSString * const IDL_PREFS_KEY_DEEPLINK_CLICK_ID = @"idl_dl_click_id";
static NSString * const IDL_PREFS_KEY_DEEPLINK_ID = @"idl_deeplink_id";
static NSString * const IDL_PREFS_KEY_REFERRER = @"idl_referrer";
static NSString * const IDL_PREFS_KEY_IDENTITY_COOKIE = @"idl_identity_cookie";
static NSString * const IDL_PREFS_KEY_USER_SDK_ID = @"idl_user_sdk_id";

static NSString * const IDL_INFO_KEY_IDL_UNIVERSAL_LINK_DOMAINS = @"instal_universal_link_domains";
static NSString * const IDL_INFO_KEY_JWT_KEYS = @"instal_keys";
static NSString * const IDL_INFO_KEY_JWT_PRIVATE_KEY = @"private_key";
static NSString * const IDL_INFO_KEY_JWT_PUBLIC_KEY = @"public_key";

@interface IDLPreferenceHelper () {
    NSOperationQueue *_persistPrefsQueue;
    NSString         *_idlAPIURL;
}

@property (strong, nonatomic) NSMutableDictionary *persistenceDict;

@end

@implementation IDLPreferenceHelper

@synthesize
deeplinkClickId = _deeplinkClickId,
deepLinkId = _deepLinkId,
referrer = _referrer,
identityCookie = _identityCookie,
userSdkID = _userSdkID,
idfa = _idfa;

+ (IDLPreferenceHelper *)preferenceHelper {
    static IDLPreferenceHelper *preferenceHelper;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        preferenceHelper = [[IDLPreferenceHelper alloc] init];
    });
    
    return preferenceHelper;
}

- (id)init {
    self = [super init];
    if (!self) return self;
    
    _persistPrefsQueue = [[NSOperationQueue alloc] init];
    _persistPrefsQueue.maxConcurrentOperationCount = 1;
    
    return self;
}

- (void) synchronize {
    [_persistPrefsQueue waitUntilAllOperationsAreFinished];
}

- (void) dealloc {
    [self synchronize];
}

#pragma mark - API methods

- (void) setIdlAPIURL:(NSString *)idlAPIURL {
    @synchronized (self) {
        _idlAPIURL = [idlAPIURL copy];
    }
}

- (NSString*) idlAPIURL {
    @synchronized (self) {
        if (!_idlAPIURL) {
            _idlAPIURL = [IDL_API_BASE_URL copy];
        }
        return _idlAPIURL;
    }
}

- (NSString *)getAPIBaseURL {
    @synchronized (self) {
        return [NSString stringWithFormat:@"%@/", self.idlAPIURL];
        // TODO: Uncomment this when the server will version their api
//        return [NSString stringWithFormat:@"%@/%@/", self.idlAPIURL, IDL_API_VERSION];
    }
}

- (NSString *)getAPIURL:(NSString *) endpoint {
    return [[self getAPIBaseURL] stringByAppendingString:endpoint];
}

- (NSString *)getEndpointFromURL:(NSString *)url {
    NSString *APIBase = self.idlAPIURL;
    if ([url hasPrefix:APIBase]) {
        NSUInteger index = APIBase.length;
        return [url substringFromIndex:index];
    }
    return @"";
}

#pragma mark - Preference Storage

- (BOOL)firstInstall {
    return [self readBoolFromDefaults:IDL_PREFS_KEY_FIRST_INSTALL defaultValue:YES];
}

- (void)setFirstInstall:(BOOL)firstInstall {
    [self writeBoolToDefaults:IDL_PREFS_KEY_FIRST_INSTALL value:firstInstall];
}

- (NSString *)deeplinkClickId {
    if (!_deeplinkClickId) {
        _deeplinkClickId = [self readStringFromDefaults:IDL_PREFS_KEY_DEEPLINK_CLICK_ID];
    }
    return _deeplinkClickId;
}

- (void)setDeeplinkClickId:(NSString *)deeplinkClickId {
    if (![_deeplinkClickId isEqualToString:deeplinkClickId]) {
        _deeplinkClickId = deeplinkClickId;
        [self writeObjectToDefaults:IDL_PREFS_KEY_DEEPLINK_CLICK_ID value:deeplinkClickId];
    }
}

- (NSString *)deepLinkId {
    if (!_deepLinkId) {
        _deepLinkId = [self readStringFromDefaults:IDL_PREFS_KEY_DEEPLINK_ID];
    }
    return _deepLinkId;
}

- (void)setDeepLinkId:(NSString *)deepLinkId {
    if (![_deepLinkId isEqualToString:deepLinkId]) {
        _deepLinkId = deepLinkId;
        [self writeObjectToDefaults:IDL_PREFS_KEY_DEEPLINK_ID value:deepLinkId];
    }
}

- (NSString *)referrer {
    if (!_referrer) {
        _referrer = [self readStringFromDefaults:IDL_PREFS_KEY_REFERRER];
    }
    return _referrer;
}

- (void)setReferrer:(NSString *)referrer {
    if (![_referrer isEqualToString:referrer]) {
        _referrer = referrer;
        [self writeObjectToDefaults:IDL_PREFS_KEY_REFERRER value:referrer];
    }
}

- (NSString *)identityCookie {
    if (!_identityCookie) {
        _identityCookie = [self readStringFromDefaults:IDL_PREFS_KEY_IDENTITY_COOKIE];
    }
    return _identityCookie;
}

- (void)setIdentityCookie:(NSString *)identityCookie {
    if (![_identityCookie isEqualToString:identityCookie]) {
        _identityCookie = identityCookie;
        [self writeObjectToDefaults:IDL_PREFS_KEY_IDENTITY_COOKIE value:identityCookie];
    }
}

- (NSString *)userSdkID {
    if (!_userSdkID) {
        _userSdkID = [self readStringFromDefaults:IDL_PREFS_KEY_USER_SDK_ID];
        if (!_userSdkID) {
             _userSdkID = [[NSUUID UUID] UUIDString];
            [self writeObjectToDefaults:IDL_PREFS_KEY_USER_SDK_ID value:_userSdkID];
        }
    }
    return _userSdkID;
}

- (NSString *)idfa {
    if (!_idfa) {
        if([[ASIdentifierManager sharedManager] isAdvertisingTrackingEnabled]) {
            NSUUID *identifier = [[ASIdentifierManager sharedManager] advertisingIdentifier];
            _idfa = [identifier UUIDString];
        } else {
            return @"";
        }
    }
    return _idfa;
}

- (id)getInstalDLUniversalLinkDomains {
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:IDL_INFO_KEY_IDL_UNIVERSAL_LINK_DOMAINS];
}

- (NSString *)getJWTPrivateKey {
    NSDictionary *keys = [[[NSBundle mainBundle] infoDictionary] objectForKey:IDL_INFO_KEY_JWT_KEYS];
    if (keys) {
        return [keys objectForKey:IDL_INFO_KEY_JWT_PRIVATE_KEY];
    }
    return nil;
}

- (NSString *)getJWTPublicKey {
    NSDictionary *keys = [[[NSBundle mainBundle] infoDictionary] objectForKey:IDL_INFO_KEY_JWT_KEYS];
    if (keys) {
        return [keys objectForKey:IDL_INFO_KEY_JWT_PUBLIC_KEY];
    }
    return nil;
}

#pragma mark - Writing To Persistence

- (void)writeIntegerToDefaults:(NSString *)key value:(NSInteger)value {
    [self writeObjectToDefaults:key value:@(value)];
}

- (void)writeBoolToDefaults:(NSString *)key value:(BOOL)value {
    [self writeObjectToDefaults:key value:@(value)];
}

- (void)writeObjectToDefaults:(NSString *)key value:(NSObject *)value {
    @synchronized (self) {
        if (value) {
            self.persistenceDict[key] = value;
        }
        else {
            [self.persistenceDict removeObjectForKey:key];
        }
        [self persistPrefsToDisk];
    }
}

- (void)persistPrefsToDisk {
    @synchronized (self) {
        if (!self.persistenceDict) return;
        NSData *data = nil;
        @try {
            data = [NSKeyedArchiver archivedDataWithRootObject:self.persistenceDict];
        }
        @catch (id exception) {
            data = nil;
            IDLLogWarning(@"Exception creating preferences data: %@.", exception);
        }
        if (!data) {
            IDLLogWarning(@"Can't create preferences data.");
            return;
        }
        NSURL *prefsURL = [self.class.URLForPrefsFile copy];
        NSBlockOperation *newPersistOp = [NSBlockOperation blockOperationWithBlock:^ {
            NSError *error = nil;
            [data writeToURL:prefsURL options:NSDataWritingAtomic error:&error];
            if (error) {
                IDLLogWarning(@"Failed to persist preferences: %@.", error);
            }
        }];
        [_persistPrefsQueue addOperation:newPersistOp];
    }
}

#pragma mark - Reading From Persistence

- (NSMutableDictionary *)persistenceDict {
    @synchronized(self) {
        if (!_persistenceDict) {
            NSDictionary *persistenceDict = nil;
            @try {
                NSError *error = nil;
                NSData *data = [NSData dataWithContentsOfURL:self.class.URLForPrefsFile
                                                     options:0 error:&error];
                if (!error && data)
                    persistenceDict = [NSKeyedUnarchiver unarchiveObjectWithData:data];
            }
            @catch (NSException*) {
                IDLLogWarning(@"Failed to load preferences from storage.");
            }
            
            if ([persistenceDict isKindOfClass:[NSDictionary class]])
                _persistenceDict = [persistenceDict mutableCopy];
            else
                _persistenceDict = [[NSMutableDictionary alloc] init];
        }
        return _persistenceDict;
    }
}

- (NSObject *)readObjectFromDefaults:(NSString *)key {
    @synchronized(self) {
        NSObject *obj = self.persistenceDict[key];
        return obj;
    }
}

- (NSString *)readStringFromDefaults:(NSString *)key {
    @synchronized(self) {
        id str = self.persistenceDict[key];
        if ([str isKindOfClass:[NSNumber class]]) {
            str = [str stringValue];
        }
        return str;
    }
}

- (BOOL)readBoolFromDefaults:(NSString *)key defaultValue:(BOOL)defaultValue {
    @synchronized(self) {
        NSNumber *number = self.persistenceDict[key];
        if (number) {
            return [number boolValue];
        }
        return defaultValue;
    }
}

- (NSInteger)readIntegerFromDefaults:(NSString *)key {
    @synchronized(self) {
        NSNumber *number = self.persistenceDict[key];
        if (number) {
            return [number integerValue];
        }
        return NSNotFound;
    }
}

#pragma mark - Preferences File URL

+ (NSURL* _Nonnull) URLForPrefsFile {
    NSURL *URL = IDLURLForIDLDirectory();
    URL = [URL URLByAppendingPathComponent:IDL_PREFS_FILE isDirectory:NO];
    return URL;
}

@end

#pragma mark - URLForIDLDirectory

NSURL* _Null_unspecified IDLCreateDirectoryForIDLURLWithPath(NSSearchPathDirectory directory) {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *URLs = [fileManager URLsForDirectory:directory inDomains:NSUserDomainMask | NSLocalDomainMask];
    
    for (NSURL *URL in URLs) {
        NSError *error = nil;
        NSURL *idlURL = [[NSURL alloc] initWithString:@"com.install" relativeToURL:URL];
        BOOL success =
        [fileManager
         createDirectoryAtURL:idlURL
         withIntermediateDirectories:YES
         attributes:nil
         error:&error];
        if (success) {
            return idlURL;
        } else  {
            IDLLogError(@"CreateIDLURL failed: %@ URL: %@.", error, idlURL);
        }
    }
    return nil;
}

NSURL* _Nonnull IDLURLForIDLDirectory() {
    NSSearchPathDirectory kSearchDirectories[] = {
        NSApplicationSupportDirectory,
        NSCachesDirectory,
        NSDocumentDirectory,
    };
    
#define _countof(array)     (sizeof(array)/sizeof(array[0]))
    
    for (NSSearchPathDirectory directory = 0; directory < _countof(kSearchDirectories); directory++) {
        NSURL *URL = IDLCreateDirectoryForIDLURLWithPath(kSearchDirectories[directory]);
        if (URL) return URL;
    }
    
#undef _countof
    
    //  Worst case backup plan:
    NSString *path = [@"~/Library/com.install" stringByExpandingTildeInPath];
    NSURL *idlURL = [NSURL fileURLWithPath:path isDirectory:YES];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error = nil;
    BOOL success =
    [fileManager
     createDirectoryAtURL:idlURL
     withIntermediateDirectories:YES
     attributes:nil
     error:&error];
    if (!success) {
        IDLLogError(@"Worst case CreateIDLURL error: %@ URL: %@.", error, idlURL);
    }
    return idlURL;
}
