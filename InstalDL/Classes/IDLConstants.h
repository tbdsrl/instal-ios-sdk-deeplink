//
//  IDLConstants.h
//  InstalDL-TestBed
//
//  Created by Leonardo Passeri on 14/02/2018.
//  Copyright © 2018 Balzo. All rights reserved.
//

#if __has_feature(modules)
@import Foundation;
#else
#import <Foundation/Foundation.h>
#endif

extern NSString * const IDL_ENDPOINT;

extern NSString * const IDL_EVENT_NAME_INSTALL;
extern NSString * const IDL_EVENT_NAME_OPEN;
extern NSString * const IDL_EVENT_NAME_REOPEN;
extern NSString * const IDL_EVENT_NAME_USER_DEFINED;

extern NSString * const IDL_SHARED_KEY_REFERRER;
extern NSString * const IDL_SHARED_KEY_DEEPLINK_CLICK_ID;
extern NSString * const IDL_SHARED_KEY_DEEPLINK_ID;
extern NSString * const IDL_SHARED_KEY_IDENTITY_COOKIE;

extern NSString * const IDL_REQUEST_KEY_USER_SDK_ID;
extern NSString * const IDL_REQUEST_KEY_IDFA;
extern NSString * const IDL_REQUEST_KEY_EVENT_TYPE;
extern NSString * const IDL_REQUEST_KEY_EVENT_VALUE;
extern NSString * const IDL_REQUEST_KEY_DEEPLINK;
extern NSString * const IDL_REQUEST_KEY_DATA;

extern NSString * const IDL_RESPONSE_KEY_PARAMS;
extern NSString * const IDL_RESPONSE_KEY_DESCRIPTION;

extern NSString * const IDL_SCHEMA_KEY_DEEPLINK_CLICK_ID;

extern NSString * const IDL_QUERY_PARAM_PUBLIC_KEY;


