//
//  IDLCallbacks.h
//  InstalDL
//
//  Created by Leonardo Passeri on 14/02/2018.
//  Copyright © 2018 Balzo. All rights reserved.
//

#if __has_feature(modules)
@import Foundation;
#else
#import <Foundation/Foundation.h>
#endif

typedef void (^callbackWithParams) (NSDictionary * _Nullable params, NSError * _Nullable error);
