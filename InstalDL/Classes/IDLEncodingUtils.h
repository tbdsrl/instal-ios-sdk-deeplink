//
//  IDLEncodingUtils.h
//  InstalDL-TestBed
//
//  Created by Leonardo Passeri on 19/02/2018.
//  Copyright © 2018 Balzo. All rights reserved.
//

#if __has_feature(modules)
@import Foundation;
#else
#import <Foundation/Foundation.h>
#endif

@interface IDLEncodingUtils : NSObject

+ (NSDictionary *)decodeQueryStringToDictionary:(NSString *)queryString;
+ (NSString *)encodeDictionaryToJWT:(NSDictionary *)payload withKey:(NSString *)key;
+ (NSString *)encodeDictionaryToQueryString:(NSDictionary *)dictionary;

@end
