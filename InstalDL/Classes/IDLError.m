//
//  IDLError.m
//  InstalDL-TestBed
//
//  Created by Leonardo Passeri on 15/02/2018.
//  Copyright © 2018 Balzo. All rights reserved.
//

#import "IDLError.h"

NSString * const IDLErrorDomain = @"io.idl.sdk.error";

@implementation NSError (InstalDL)

+ (NSString*) messageForCode:(IDLErrorCode)code {
    
    // The order is important!
    
    static NSString* const messages[] = {
        
        // IDLBadRequestError
        @"The network request was invalid.",
        
        // IDLRequestEncodeError
        @"Unable to encode client request",
        
        // IDLServerProblemError
        @"Trouble reaching the Instal servers, please try again shortly.",
        
        // IDLNilLogError
        @"Can't log error messages because the logger is set to nil.",
        
        // IDLResponseParseError
        @"Unable to deserialize server response",
        
        // IDLConnectivityError
        @"NSURLErrorDomain error",
        
        // IDLMissingPrivateKeyError
        @"Can't find private key",
        
        // IDLMissingPublicKeyError
        @"Can't find public key",
        
        // IDLUnexpectedError
        @"An unexpected error has occurred",
    };
    
#define _countof(array) (sizeof(array)/sizeof(array[0]))
    
    // Sanity check
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunreachable-code"
    if (_countof(messages) != (IDLHighestError - IDLBadRequestError)) {
        [NSException raise:NSInternalInconsistencyException format:@"InstalDL error message count is wrong."];
        return @"InstalDL encountered an error.";
    }
#pragma clang diagnostic pop
    
    if (code < IDLBadRequestError || code >= IDLHighestError)
        return @"InstalDL encountered an error.";
    
    return messages[code - IDLBadRequestError];
}

+ (NSError*_Nonnull) idlErrorWithCode:(IDLErrorCode)errorCode
                                   error:(NSError*)error
                        localizedMessage:(NSString*_Nullable)message {
    
    NSMutableDictionary *userInfo = [NSMutableDictionary new];
    
    NSString *localizedString = [self messageForCode:errorCode];
    if (localizedString) userInfo[NSLocalizedDescriptionKey] = localizedString;
    if (message) {
        userInfo[NSLocalizedFailureReasonErrorKey] = message;
    }
    if (error) {
        userInfo[NSUnderlyingErrorKey] = error;
        if (!userInfo[NSLocalizedFailureReasonErrorKey] && error.localizedDescription)
            userInfo[NSLocalizedFailureReasonErrorKey] = error.localizedDescription;
    }
    
    return [NSError errorWithDomain:IDLErrorDomain code:errorCode userInfo:userInfo];
}

+ (NSError*_Nonnull) idlErrorWithCode:(IDLErrorCode)errorCode {
    return [NSError idlErrorWithCode:errorCode error:nil localizedMessage:nil];
}

+ (NSError*_Nonnull) idlErrorWithCode:(IDLErrorCode)errorCode error:(NSError*_Nullable)error {
    return [NSError idlErrorWithCode:errorCode error:error localizedMessage:nil];
}

+ (NSError*_Nonnull) idlErrorWithCode:(IDLErrorCode)errorCode localizedMessage:(NSString*_Nullable)message {
    return [NSError idlErrorWithCode:errorCode error:nil localizedMessage:message];
}

@end
