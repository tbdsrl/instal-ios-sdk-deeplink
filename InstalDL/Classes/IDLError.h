//
//  IDLError.h
//  InstalDL-TestBed
//
//  Created by Leonardo Passeri on 15/02/2018.
//  Copyright © 2018 Balzo. All rights reserved.
//

#if __has_feature(modules)
@import Foundation;
#else
#import <Foundation/Foundation.h>
#endif

FOUNDATION_EXPORT NSString *_Nonnull const IDLErrorDomain;

typedef NS_ENUM(NSInteger, IDLErrorCode) {
    IDLBadRequestError              = 1001,
    IDLRequestEncodeError           = 1002,
    IDLServerProblemError           = 1003,
    IDLNilLogError                  = 1004,
    IDLResponseParseError           = 1005,
    IDLConnectivityError            = 1006,
    IDLMissingPrivateKeyError       = 1007,
    IDLMissingPublicKeyError        = 1008,
    IDLUnexpectedError              = 1009,
    IDLHighestError,
};

@interface NSError (InstalDL)
+ (NSError*_Nonnull) idlErrorWithCode:(IDLErrorCode)errorCode;
+ (NSError*_Nonnull) idlErrorWithCode:(IDLErrorCode)errorCode error:(NSError*_Nullable)error;
+ (NSError*_Nonnull) idlErrorWithCode:(IDLErrorCode)errorCode localizedMessage:(NSString*_Nullable)message;
@end
