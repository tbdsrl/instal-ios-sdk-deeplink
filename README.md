# InstalDL

[![Carthage compatible](https://img.shields.io/badge/Carthage-compatible-4BC51D.svg?style=flat)](https://github.com/Carthage/Carthage)
[![Version](https://img.shields.io/cocoapods/v/InstalDL.svg?style=flat)](http://cocoapods.org/pods/InstalDL)
[![License](https://img.shields.io/cocoapods/l/InstalDL.svg?style=flat)](http://cocoapods.org/pods/InstalDL)
[![Platform](https://img.shields.io/cocoapods/p/InstalDL.svg?style=flat)](http://cocoapods.org/pods/InstalDL)

## iOS Reference

* Getting started
    * [Library installation](#markdown-header-installation)
    * [Register your app](#markdown-header-register-your-app)
    * [Add your keys](#markdown-header-add-your-instal-keys-to-your-project)
    * [Register a URI scheme](#markdown-header-register-a-uri-scheme-direct-deep-linking-optional-but-recommended)
    * [Support Universal Links](#markdown-header-support-universal-linking)

* InstalDL general methods
    * [Get an InstalDL singleton](#markdown-header-get-a-singleton-instaldl-instance)
    * [Initialize InstalDL and register deep link router](#markdown-header-init-instaldl-session-and-deep-link-routing-function)
    * [Tracking User Actions and Events](#markdown-header-tracking-user-actions-and-events)

## Installation

### Available in CocoaPods

InstalDL is available through [CocoaPods](http://cocoapods.org). To install it, simply add the following line to your Podfile:

```ruby
pod 'InstalDL'
```

Then, from the command line, `cd` to your project directory, and do:

```
pod install
pod update
```

to install the InstalDL pod and update it to the latest version of the SDK.

Make sure to do the `pod update`.  CocoaPods may not use the latest version of the SDK otherwise!

### Carthage

To integrate InstalDL into your project using Carthage add the following to your `Cartfile`:

```ruby
git "https://bitbucket.org/tbdsrl/instal-ios-sdk-deeplink.git"
```
Then , from the command line, `cd` to your project directory, and do:
```
carthage update --platform iOS
```
Lastly, follow the rest of the [standard Carthage installation instructions](https://github.com/Carthage/Carthage#adding-frameworks-to-an-application) to add `InstalDL`, `JWT` and `Base64` frameworks to your project.

### Register Your App

You can sign up for your own app id at [https://dpl.instal.com](https://dpl.instal.com).

### Add Your Instal Keys to Your Project

After you register your app, a private-public key pair will be available to you in the App Settings of your app.

![Instal Keys Dashboard](docs/images/instal-keys-dashboard.png)

You need to add these keys to YourProject-Info.plist (Info.plist for Swift).

1. In plist file, mouse hover "Information Property List," which is the root item under the Key column.
1. After about half a second, you will see a "+" sign appear. Click it.
1. In the newly added row, fill in "instal_keys" for its key, change type to Dictionary, and add two entries inside:
1. For the private key, use "private_key" (without double quotes) for key, String for type, and your private key for value.
2. For the public key, use "public_key" (without double quotes) for key, String for type, and your public key for value.

![Instal Keys Plist](docs/images/instal-keys-plist.png)

### Register a URI Scheme Direct Deep Linking (Optional but Recommended)

You can register your app to respond to direct deep links (yourapp:// in a mobile browser) by adding a URI scheme in your project's Info page. Make sure to change **yourapp** to a unique string that represents your app name.

1. In Xcode, click your project in the Navigator (on the left side).
1. Select the "Info" tab.
1. Expand the "URL Types" section at the bottom.
1. Click the "+" sign to add a new URI Scheme, as below:

![URL Scheme Demo](docs/images/url-scheme.png)

### Support Universal Linking

With iOS 9, Apple has added the ability to allow http links to directly open your app, rather than using the URI Schemes. This can be a pain to set up, as it involves a complicated process on your server. The good news is that Instal does this work for you with just two steps!

1. In Xcode, click on your project in the Navigator (on the left side).
1. Select the "Capabilities" tab.
1. Expand the "Associated Domains" tab.
1. Enable the setting (toggle the switch).
1. Add `applinks:xxxx.instal.com` and `applinks:alternative-xxxx.instal.com` to the list. Make sure `xxxx` matches the subdomain for your app. You can find it on the dashboard here:

![Dashboard Domains UL](docs/images/dashboard-ul-domains.png)

![Xcode Enable UL](docs/images/xcode-ul-enable.png)

Add any additional custom domains you have (e.g. `applinks:example.com`)

1. On the Dashboard, navigate to your app's link settings page.
1. Check the "Enable Universal link - iOS"
1. Insert your Apple App Prefix and app Bundle Identifier.
1. Be sure to save these settings updates.

![Dashboard Enable UL](docs/images/dashboard-ul-enable.png)

You can find both your App Prefix and Bundle Identifier in the _Certificates, Identifiers and Profiles_ section of your developer account:

![IOS Apple Developer Account](docs/images/ios-apple-developer-account.png)

#### Custom Domain Name Configuration (Required if you don't use the Instal provided xxxx.instal.com domain)

Instal provides a xxxx.instal.com domain for your app, but you can use your own custom domain for app links instead. If you _do_ use your own custom domain for your universal app links, you need to add it to your Info.plist.

Add the `instal_universal_link_domains` key with your custom domain as a string value:

![Custom Domain Info.plist](docs/images/custom-domain.png)

### Get a Singleton InstalDL Instance

All InstalDL methods require an instance of the main InstalDL object. Here's how you can get one. It's stored statically and is accessible from any class.

#### Methods

###### Objective-C

```objc
InstalDL *instalDL = [InstalDL getInstance];
```

###### Swift

```swift
let instalDL: InstalDL = InstalDL.getInstance()
```

### Init InstalDL Session and Deep Link Routing Function

To deep link, InstalDL must initialize a session to check if the user originated from a link. This call will initialize a new session _every time the app is launched_. Everytime the user opens the app through a link, InstalDL will call the deep link handling block. By accessing the keys in the params, you can route the user depending on the data you passed in.

#### Methods

###### Objective-C
```objc
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
InstalDL *instalDL = [InstalDL getInstance];
[instalDL initSessionWithLaunchOptions:launchOptions andRegisterDeepLinkHandler:^(NSDictionary *params, NSError *error) {
// route the user based on what's in params
}];
return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {

BOOL instalDLHandled =
[[InstalDL getInstance]
application:application
openURL:url
sourceApplication:sourceApplication
annotation:annotation];

if (!instalDLHandled) {
// do other deep link routing for the Facebook SDK, Pinterest SDK, etc
}
return YES;
}

- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray *restorableObjects))restorationHandler {
BOOL handledByInstalDL = [[InstalDL getInstance] continueUserActivity:userActivity];

return handledByInstalDL;
}
```

###### Swift

```swift
func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
let instalDL: InstalDL = InstalDL.getInstance()
instalDL.initSession(launchOptions: launchOptions, deepLinkHandler: { params, error in
if error == nil {
// params are the deep linked params associated with the link that the user clicked -> was re-directed to this app
print("params: %@", params.description)
}
})
return true
}

func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {

// Pass the url to the handle deep link call
let instalDLHandled = InstalDL.getInstance().application(application,
open: url,
sourceApplication: sourceApplication,
annotation: annotation
)
if (!instalDLHandled) {
// If not handled by InstalDL, do other deep link routing for the
// Facebook SDK, Pinterest SDK, etc
}

return true
}

func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
InstalDL.getInstance().continue(userActivity)

return true
}
```


Note:  If your application delegate declares the method:

```
- (BOOL) application:willFinishLaunchingWithOptions:
```

In Swift:

```
optional func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool
```

it must return ```YES``` for InstalDL to work.


#### Parameters

###### initSession

**launchOptions** (NSDictionary *) _required_
: These launch options are passed to InstalDL through didFinishLaunchingWithOptions and will notify us if the user originated from a URI call or not. If the app was opened from a URI like myapp://, we need to follow a special initialization routine.

**deepLinkHandler** ^(NSDictionary *params, NSError *error) _optional_
: This is the callback block that InstalDL will execute after a network call to determine where the user comes from.

- _NSDictionary *params_ : These params will contain any data associated with the InstalDL link that was clicked.
- _NSError *error_ : This error will be nil unless there is an error such as connectivity or otherwise. Check !error to confirm it was a valid link.

###### continueUserActivity

**userActivity** (NSUserActivity *) _required_
: This argument passes us the user activity so that we can parse the originating URL.

#### Returns

###### initSession

Nothing

###### continueUserActivity

**BOOL** continueUserActivity will return a boolean indicating whether InstalDL has handled the Universal Link. If Universal Link is powered by Instal, then continueUserActivity will return YES because the Instal click object is present.

### Tracking User Actions and Events

You can send custom events to track special user actions or application specific events beyond app installs and opens. You can track events such as when a user adds an item to an on-line shopping cart, or searches for a keyword, among others. You can specify an event type as a String value (except for reserved events: `install`, `open`, `reopen`) and an optional event value:

###### Objective-C

```objc
[[InstalDL getInstance] sendEventType:@"User_Purchased_Item" withValue:@(1)];
```

###### Swift

```swift
InstalDL.getInstance().sendEventType("User_Purchased_Item", withValue: 1)
```

