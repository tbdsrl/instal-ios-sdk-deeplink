#
# Be sure to run `pod lib lint InstalDL.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'InstalDL'
  s.version          = '1.0.0'
  s.summary          = 'Deeplink SDK for the Instal platform'
  s.description      = <<-DESC
InstalDL is an SDK which allows to easily manage deeplinks through Universal links and URI schemes from the Instal platform
                       DESC
  s.homepage         = 'https://bitbucket.org/tbdsrl/instal-ios-sdk-deeplink'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Instal' => 'support@instal.com' }
  s.source           = { :git => 'https://bitbucket.org/tbdsrl/instal-ios-sdk-deeplink.git', :tag => s.version.to_s }

  s.platform         = :ios, '10.0'

  s.source_files = 'InstalDL/Classes/**/*'
  s.dependency 'JWT', '2.2.0'
end
